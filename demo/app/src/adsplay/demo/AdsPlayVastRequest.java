package adsplay.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

import net.adsplay.util.VASTLog;
import net.adsplay.vast.VASTPlayer;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
 
public class AdsPlayVastRequest extends AsyncTask<Activity, Void, String> {
	private final static String TAG = "AdsPlayVastRequest";
	Activity activity;
	String url;
	VASTPlayer newPlayer;
	String vastXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><VAST version=\"2.0\"></VAST>";
	
	
	@Override
	protected String doInBackground(Activity... params) {		
		this.activity = params[0];
		this.url = getVastUrl();
		return doHttpGet();
	}
	
	
	String getVastUrl(){
		int zoneId = 1001;
		int placementId = 105;		
		StringBuilder url = new StringBuilder("http://report.adsplay.net/delivery/zone/");
		url.append(zoneId).append(".xml?app=1&placement=").append(placementId);
		String uuid = Installation.id(activity.getApplicationContext());
		url.append("&uuid=").append(uuid);
		url.append("&t=").append(System.currentTimeMillis());
		return url.toString();
	}
	
	
	final String doHttpGet()
	{		
		BufferedReader inStream = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			
			HttpGet httpRequest = new HttpGet(url);
			HttpResponse response = httpClient.execute(httpRequest);
			inStream = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer buffer = new StringBuffer("");
			String line = "";			
			while ((line = inStream.readLine()) != null) {
				buffer.append(line);
			}
			inStream.close();
			vastXML = buffer.toString();			
		} catch (Exception e) {			
			e.printStackTrace();
			VASTLog.e(TAG,e.getMessage());
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return vastXML;
	}
	
	protected void onPostExecute(String vastXMLContent)
	{    	
    	  Log.i(TAG, vastXMLContent);
    	  newPlayer = new VASTPlayer(activity,
    			  new VASTPlayer.VASTPlayerListener() {			
					@Override
					public void vastReady() {
						VASTLog.i(TAG,"VAST Document is ready and we can play it now");
						newPlayer.play();
					}					
					@Override
					public void vastError(int error) {
					    String message = "Unable to play VAST Document: Error: " + error;
					    VASTLog.e(TAG,  message);
					}					
					@Override
					public void vastClick() {
					    VASTLog.e(TAG, "VAST click event fired");
					}
					
					@Override
					public void vastComplete() {
						VASTLog.e(TAG, "VAST complete event fired");
						
					}
					
					@Override
					public void vastDismiss() {
						VASTLog.e(TAG, "VAST dismiss event fired");
					}
    	  });
    	  newPlayer.loadVideoWithData(vastXMLContent);
	}	
}
